import mongooseConnecter from "./mongooseConnecter.js";

import "dotenv/config";

describe("Test mongooseConnecter function with wrong DB_URL", () => {
  const OLD_ENV = process.env;

  beforeEach(() => {
    jest.resetModules(); // Most important - it clears the cache
    process.env = { ...OLD_ENV }; // Make a copy
  });

  afterEach(() => {
    jest.resetModules(); // Most important - it clears the cache
    process.env = OLD_ENV; // Restore old environment
  });

  test("It should response the GET method", async () => {
    process.env.DB_URL = "dev";
    const result = await mongooseConnecter();
    expect(result).toBe("Please define the DB_URL environment variable inside .env");
  });
});

describe("Test mongooseConnecter function", () => {
  test("It should response the GET method", async () => {
    await mongooseConnecter();
  });
});
