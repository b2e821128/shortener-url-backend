import mongoose from "mongoose";

const mongooseConnecter = async () => {
  const DB_URL = process.env.DB_URL;

  let cached = global.mongoose;

  if (!DB_URL || !DB_URL.includes("mongodb")) {
    return "Please define the DB_URL environment variable inside .env";
  }

  if (!cached) {
    cached = global.mongoose = { conn: null, promise: null };
  }

  if (cached.conn) {
    return cached.conn;
  }

  if (!cached.promise) {
    const options = { useNewUrlParser: true, useUnifiedTopology: true };
    cached.promise = mongoose.connect(DB_URL, options).then((mongoose) => {
      return mongoose;
    });
  }
  cached.conn = await cached.promise;
  return cached.conn;
};

export default mongooseConnecter;
