import express from "express";
import cors from "cors";
import ShortUniqueId from "short-unique-id";
import "dotenv/config";

import mongooseConnecter from "./utils/mongooseConnecter.js";
import ShortUrl from "./models/shortUrl.js";

const app = express();

app.use(express.urlencoded({ extended: false }));
app.use(express.json());
app.use(cors());

app.get("/", async (req, res) => {
  res.status(200).json({ msg: "url shortener backend api deploy on vercel 1.0.5" });
});

app.get("/shortUrls/query", async (req, res) => {
  await mongooseConnecter();
  const shortUrls = await ShortUrl.find();
  res.status(200).json({ shortUrls: shortUrls });
});

app.post("/shortUrls/create", async (req, res) => {
  await mongooseConnecter();
  const uid = new ShortUniqueId();
  const uidWithTimestamp = uid.stamp(32);

  await ShortUrl.create({ full: req.body.fullUrl, short: `https://redirecturl.netlify.app/?token=${uidWithTimestamp}`, key: uidWithTimestamp });
  res.status(201).json({ msg: "created successful" });
});

app.post("/shortUrls/delete", async (req, res) => {
  await mongooseConnecter();
  const result = await ShortUrl.deleteOne({ _id: req.body.shortUrlId });

  res.status(202).json({ msg: "delete successful", result });
});

app.post("/shortUrls/decode", async (req, res) => {
  await mongooseConnecter();
  const shortUrl = await ShortUrl.findOne({ key: req.body.shortUrl });
  if (shortUrl == null) return res.status(404).json({ msg: "shortUrl invaild" });
  shortUrl.clicks++;
  shortUrl.save();
  res.status(200).json({ destinationURL: shortUrl.full });
});

export default app;
