import app from "./app.js";

app.listen(process.env.PORT || 3000, () => {
  const PORT = process.env.PORT ? process.env.PORT : 3000;
  console.log(`Example app listening on port ${PORT} !`);
});
